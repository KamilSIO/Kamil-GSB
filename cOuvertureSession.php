<?php
/** 
 * Regroupe les fonctions de gestion d'une session utilisateur.
 * @package default
 * @todo  RAS
 */

 /** Démarre session.                     
 * @return void
 */
 session_start();
/* Connexion à une base MySQL avec l'invocation de pilote */
$dsn = 'mysql:dbname=gsb_frais_droit;host=127.0.0.1';
$user = 'root';
$password = '';

try {
    $dbh = new PDO($dsn, $user, $password);
} catch (PDOException $e) {
    echo 'Connexion échouée : ' . $e->getMessage();
} 

echo 'connexion réussie : ';

$login = 'Christian';
$motdepasse = 'wallon2017';


die();
  

/** 
 * Retourne l'id de l'utilisateur connecté, une chaîne vide si pas de visiteur connecté.
 * @return string id de l'utilisateur connecté
 */
function obtenirIdUtilisateurConnecte() {
    $ident="";
    if ( isset($_SESSION["loginUtilisateur"]) ) {
        $ident = (isset($_SESSION["idUtilisateur"])) ? $_SESSION["idUtilisateur"] : '';   
    }  
    return $ident ;
}

/**
 * Conserve en variables session l'id $id et le login $login de l'utilisateur connecté
 * @param string id de l'utilisateur
 * @param string login de l'utilisateur
 * @return void    
 */
function affecterInfosConnecte($id, $nom) {
    $_SESSION["idUtilisateur"] = $id;
    $_SESSION["loginUtilisateur"] = $nom;

/**
* Va permettre de cocher les groupes de l'utilisateurs
*//* Connexion à une base MySQL avec l'invocation de pilote */

if ($groupe == 1)
{
  $Administrateur_checked = "";
  $Utilisateur_checked = "checked = 'checked'";
  
}
else
{
  $Utilisateur_checked = "";
  $Administrateur_checked = "checked = 'checked'";
}
/*else*/
{
$Utilisateur_checked = "checked = 'checked'";
  $Administrateur_checked = "checked = 'checked'"; 
}
}

/**
*Conserve en variables session les informations sur les droits du groupe de l'utilisateur
* @param string id du groupe
 * @param string  nom du groupe
*/
function affecterDroitsGroupe($id, $nom ){
    $_SESSION["idDroit"] = $id;
    $_SESSION["nomDroit"] = $nom;

}

/** 
 * Déconnecte l'utilisateur qui s'est identifié sur le site.                     
 *
 * @return void
 */
function deconnecterVisiteur() {
    unset($_SESSION["idUtilisateur"]);
    unset($_SESSION["loginUtilisateur"]);
}

/** 
 * Vérifie si un utilisateur s'est connecté sur le site.                     
 *
 * Retourne true si un utilisateur s'est identifié sur le site, false sinon. 
 * @return boolean échec ou succès
 */
function estVisiteurConnecte() {
    // actuellement il n'y a que les utilisateurs qui se connectent
    return isset($_SESSION["loginUtilisateur"]);
}
?>